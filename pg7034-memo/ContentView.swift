//
//  ContentView.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var  memoViewModel = MemoViewModel()
    @ObservedObject var locationManager = LocationManager()

    private var memos: [Memo] {
        return memoViewModel.memos
        
    }
    
    init(){
        let appearance = UINavigationBarAppearance()
        appearance.configureWithTransparentBackground()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x545454)]
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
    }
    
    @ViewBuilder
    var body: some View {
        NavigationView {
            ZStack {
                Color(hex: 0xffd9fe)
                    .edgesIgnoringSafeArea(.all)
                VStack(){
                    NavigationLink(destination: NewMemoView(memoViewModel: self.memoViewModel)
                        .navigationBarTitle("New Memo", displayMode: .inline)
                        .navigationBarItems(
                            leading:BackButton()
                        )
                        .navigationBarBackButtonHidden(true)
                    ){
                        Text("New Memo")
                            .fontWeight(.bold)
                            .padding()
                            .background(Color.purple)
                            .cornerRadius(CGFloat(40))
                            .foregroundColor(.white)
                            .padding(10)
                        }
                    if memos.isEmpty {
                        Spacer()
                        Text("No Memos to show")
                        Spacer()
                    } else {
                        GeometryReader { geometry in
                            ScrollView {
                                VStack(spacing: 20){
                                ForEach(self.memos, id: \.self) { memo in
                                    NavigationLink(
                                        destination:
                                        MemoDetailView(locationManager: self.locationManager, memo: memo, width:   UIScreen.main.bounds.width-20)
                                            .navigationBarTitle("Memo Details",     displayMode: .inline)
                                                .navigationBarItems(
                                                    leading:BackButton(),
                                                    trailing:
                                                    Button(
                                                        action: {self.memoViewModel.deleteMemo(memo: memo)},
                                                        label:{
                                                            Image(systemName: "trash.fill").foregroundColor(Color.purple)
                                                        }
                                                    )
                                                    .navigationBarBackButtonHidden(true)
                                                )
                                    ){
                                        MemosRowView(
                                        memo: memo,
                                        width: UIScreen.main.bounds.width-60,
                                        height: 200)
                                    }.cornerRadius(CGFloat(15))
                                }
                            }.onAppear {
                                UITableView.appearance().separatorStyle = .none
                            }.frame(width: UIScreen.main.bounds.width-60)
                            }
                        }
                    }
                }.navigationBarTitle(Text("My Memos"), displayMode: .inline)
            }
        }.onAppear(perform: getNotificationsAuthorization)
    }
    
    func getNotificationsAuthorization(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("All set!")
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
}
