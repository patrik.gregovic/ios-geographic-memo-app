//
//  LocationManager.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/15/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
import Combine

class LocationManager: NSObject, ObservableObject {
    lazy var geocoder = CLGeocoder()
    
    @Published var locationString = ""
    @Published var invalid: Bool = false
   
    func openMapWithAddress (address: String) {
        self.getLocation(from: address) { coordinates in
            if let coords = coordinates {
                let place = MKPlacemark(coordinate: coords)
                let mapItem = MKMapItem(placemark: place)
                mapItem.name = address
                mapItem.openInMaps(launchOptions: nil)
            } else {
                DispatchQueue.main.async {
                    self.invalid = true
                }
            }
        }
        /*geocoder.geocodeAddressString(locationString) { placemarks, error in
            if let error = error {
                DispatchQueue.main.async {
                    self.invalid = true
                }
                print(error.localizedDescription)
            }
            
            guard let placemark = placemarks?.first else {
                return
            }
            
            guard let lat = placemark.location?.coordinate.latitude else{return}
            
            guard let lon = placemark.location?.coordinate.longitude else{return}
            
            let coords = CLLocationCoordinate2DMake(lat, lon)
            
            let place = MKPlacemark(coordinate: coords)
            
            let mapItem = MKMapItem(placemark: place)
            mapItem.name = self.locationString
            mapItem.openInMaps(launchOptions: nil)
        }*/
        
    }
    
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,
            let location = placemarks.first?.location?.coordinate else {
                completion(nil)
                return
            }
            completion(location)
        }
    }
}
