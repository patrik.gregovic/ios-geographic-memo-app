//
//  Memo.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import Foundation

struct Memo: Codable, Identifiable, Hashable {
    let id = UUID()
    let title: String
    let date: Date
    let note: String
    let location: String
}

