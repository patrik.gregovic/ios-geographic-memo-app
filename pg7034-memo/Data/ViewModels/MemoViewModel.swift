//
//  MemoViewModel.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import Foundation
import UserNotifications

final class MemoViewModel: ObservableObject {
    
    @Published private(set) var memos: [Memo] = []
    private let defaults = UserDefaults.standard
    
    init() {
        getMemos()
    }
    
    func getMemos(){
        self.memos = []
        self.load()
    }
    
    func addMemo(memo: Memo){
        self.memos.append(memo)
        self.save()
        self.setNotification(memo: memo)
    }
    
    func deleteMemo(memo: Memo) {
        if let index = memos.firstIndex(where: {$0.id == memo.id}) {
            memos.remove(at: index)
            self.save()
            self.cancelNotification(memo: memo)
        }
    }
    
    func save() {
        if let encoded = try? JSONEncoder().encode(self.memos) {
            UserDefaults.standard.set(encoded, forKey: "memos")
        }
    }

    func load() {
        do {
            if let storedObjItem = UserDefaults.standard.object(forKey: "memos") {
                self.memos = try JSONDecoder().decode([Memo].self, from: storedObjItem as! Data)
            }
        } catch let err {
            print(err)
        }
    }
    
    func setNotification(memo: Memo){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        let memotime = dateFormatter.string(from: memo.date)
        
        let content = UNMutableNotificationContent()
        content.title = memo.title
        content.subtitle = memotime
        content.sound = UNNotificationSound.default

        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: memo.date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        // choose a random identifier
        let request = UNNotificationRequest(identifier: memo.id.uuidString, content: content, trigger: trigger)

        // add our notification request
        UNUserNotificationCenter.current().add(request)
    }
    
    func cancelNotification(memo: Memo){
        UNUserNotificationCenter.current()
            .removePendingNotificationRequests(withIdentifiers: [memo.id.uuidString])

    }
    
}
