//
//  NewMemoView.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct NewMemoView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    
    @ObservedObject var  memoViewModel: MemoViewModel
    @State private var titleInput: String = String()
    @State private var dateInput: Date = Date()
    @State private var noteInput: String = String()
    @State private var locationInput: String = String()
    @State private var showSuccessAlert = false
    @State private var successMessage = ""
    @State private var successMessageDetails = ""
    @State private var disableInputs = false


    
    var body: some View {
        ZStack {
            Color(hex: 0xffd9fe)
            .edgesIgnoringSafeArea(.all)
            VStack(alignment: .center){
                VStack(alignment: .leading){
                    Text("TITLE")
                    TextField("Enter title", text: $titleInput)
                    .foregroundColor(.purple)
                    .disabled(disableInputs)
                    .padding()
                    .background(RoundedRectangle(cornerRadius: CGFloat(10)).fill(Color.white))
                    .overlay(RoundedRectangle(cornerRadius: CGFloat(10))
                        .strokeBorder( Color.gray, style: StrokeStyle(lineWidth: CGFloat(1)))
                    )
                }.padding()
                VStack(alignment: .leading){
                        Text("DATE")
                        DatePicker(
                            "",
                            selection: $dateInput,
                            in: Date()...,
                            displayedComponents: [.date, .hourAndMinute]
                            )
                        .colorInvert()
                        .colorMultiply(Color.purple)
                        .disabled(disableInputs)
                        .labelsHidden()
                        .frame(width: UIScreen.main.bounds.width-20)
                        .background(RoundedRectangle(cornerRadius: CGFloat(10)).fill(Color.white))
                        .overlay(RoundedRectangle(cornerRadius: CGFloat(10))
                            .strokeBorder( Color.gray, style: StrokeStyle(lineWidth: CGFloat(1)))
                        )
                }.padding()
                VStack(alignment: .leading){
                        Text("NOTE")
                        TextField("Enter note", text: $noteInput)
                        .foregroundColor(.purple)
                        .disabled(disableInputs)
                        .padding()
                        .background(RoundedRectangle(cornerRadius: CGFloat(10)).fill(Color.white))
                        .overlay(RoundedRectangle(cornerRadius: CGFloat(10))
                            .strokeBorder( Color.gray, style: StrokeStyle(lineWidth: CGFloat(1)))
                        )
                }.padding()
                VStack(alignment: .leading){
                        Text("LOCATION")
                        TextField("Enter location", text: $locationInput)
                        .foregroundColor(.purple)
                        .disabled(disableInputs)
                        .padding()
                        .background(RoundedRectangle(cornerRadius: CGFloat(10)).fill(Color.white))
                        .overlay(RoundedRectangle(cornerRadius: CGFloat(10))
                            .strokeBorder( Color.gray, style: StrokeStyle(lineWidth: CGFloat(1)))
                        )
                }.padding()
                
                Button(action: addMemo){
                    Text("Add to My Memos")
                    .fontWeight(.bold)
                    .padding()
                    .background(Color.purple)
                    .cornerRadius(CGFloat(40))
                    .foregroundColor(.white)
                    .padding(10)
                }.disabled(disableInputs)
            }
        }.alert(isPresented: $showSuccessAlert) {
            Alert(
                title: Text(self.successMessage),
                message: Text(self.successMessageDetails)
            )
        }
    }
    
    func addMemo(){
        var missingInputs: [String] = []
        if( self.titleInput.isEmpty ) {missingInputs.append("title")}
        if( self.noteInput.isEmpty ) {missingInputs.append("note")}
        if( self.locationInput.isEmpty ) {missingInputs.append("location")}
        if(!missingInputs.isEmpty){
            self.successMessage="Uh oh!"
            self.successMessageDetails="Your memo was not added. You are missing: "+missingInputs.joined(separator: ", ")
        } else {
            let newMemo = Memo(
                title: self.titleInput,
                date: self.dateInput,
                note: self.noteInput,
                location: self.locationInput
            )
            print(newMemo.date)
            self.memoViewModel.addMemo(memo: newMemo)
            self.successMessage="Success!"
            self.successMessageDetails="Your memo was added succcessfully."
            disableInputs = true
            self.presentationMode.wrappedValue.dismiss()
        }
        showSuccessAlert = true
    }
}
