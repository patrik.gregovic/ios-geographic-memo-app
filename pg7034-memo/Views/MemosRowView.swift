//
//  MemosRowView.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct MemosRowView: View {
    
    var memo: Memo
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            HStack(){
                Text("Title:")
                    .font(.body)
                    .foregroundColor(Color(hex: 0xe0e0e0))
                    .fontWeight(.medium)
                Spacer()
                Text(memo.title)
                    .font(.body)
                    .foregroundColor(Color.purple)
                    .fontWeight(.medium)
            }
            HStack(){
                Text("Date:")
                    .font(.body)
                    .foregroundColor(Color(hex: 0xe0e0e0))
                    .fontWeight(.medium)
                Spacer()
                Text(self.getDateAsString(date: memo.date))
                    .font(.body)
                    .foregroundColor(Color.purple)
                    .fontWeight(.medium)
            }
            HStack(){
                Text("Note:")
                    .font(.body)
                    .foregroundColor(Color(hex: 0xe0e0e0))
                    .fontWeight(.medium)
                Spacer()
                Text(memo.note)
                    .font(.body)
                    .foregroundColor(Color.purple)
                    .fontWeight(.medium)
            }
        }
        .padding(30)
        .frame(width: self.width, height: self.height)
        .background(Color.white)
        .cornerRadius(12)
    }
    
    func getDateAsString(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)
    }
    
}
