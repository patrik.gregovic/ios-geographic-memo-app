//
//  MemoDetailView.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct MemoDetailView: View {
    
    @ObservedObject var locationManager: LocationManager
    
    var memo: Memo
    var width: CGFloat
    
    var body: some View {
        ZStack {
            Color(hex: 0xffd9fe)
                .edgesIgnoringSafeArea(.all)
            VStack(alignment: .leading, spacing: CGFloat(15)){
                Group{
                    Text("Title")
                        .font(.body)
                        .foregroundColor(Color(hex: 0xe0e0e0))
                        .fontWeight(.medium)
                    Text(memo.title)
                        .font(.body)
                        .foregroundColor(Color.purple)
                        .fontWeight(.medium)
                }
                Spacer()
                Group{
                    Text("Date")
                        .font(.body)
                        .foregroundColor(Color(hex: 0xe0e0e0))
                        .fontWeight(.medium)
                    Text(self.getDateAsString(date: memo.date))
                        .font(.body)
                        .foregroundColor(Color.purple)
                        .fontWeight(.medium)
                    
                }
                Spacer()
                Group{
                    Text("Note")
                        .font(.body)
                        .foregroundColor(Color(hex: 0xe0e0e0))
                        .fontWeight(.medium)
                    Text(memo.note)
                        .font(.body)
                        .foregroundColor(Color.purple)
                        .fontWeight(.medium)
                }
                Spacer()
                Group{
                    Text("Location")
                        .font(.body)
                        .foregroundColor(Color(hex: 0xe0e0e0))
                        .fontWeight(.medium)
                    Text(memo.location)
                        .font(.body)
                        .foregroundColor(Color.purple)
                        .fontWeight(.medium)
                }
                Spacer()
                HStack() {
                    Spacer()
                    Button( action: { self.locationManager.openMapWithAddress(address: self.memo.location) }){
                        Text("Show on map")
                            .fontWeight(.bold)
                            .padding()
                            .background(Color.purple)
                            .cornerRadius(CGFloat(15))
                            .foregroundColor(.white)
                            .padding(10)
                    }
                    Spacer()
                }
            }
            .padding(45)
            .frame(width: self.width, height: UIScreen.main.bounds.height-350).cornerRadius(CGFloat(12))
            .background(Color.white)
            .cornerRadius(CGFloat(15))
        }.alert(isPresented: $locationManager.invalid) {
            Alert(title: Text("Error"), message: Text("The address is invalid"), dismissButton: .default(Text("OK"), action:{
                self.locationManager.invalid = false
                }))
            }
    }
    
    func getDateAsString(date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)
    }
}
