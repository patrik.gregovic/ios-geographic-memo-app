//
//  BackButton.swift
//  pg7034-memo
//
//  Created by Patrik Gregovic (RIT Student) on 12/14/21.
//  Copyright © 2021 Patrik Gregovic (RIT Student). All rights reserved.
//

import SwiftUI

struct BackButton: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack(){
                Rectangle()
                    .foregroundColor(Color.white)
                    .background(Color.white)
                    .cornerRadius(50)
                    .frame(width:30, height:30)
                    .overlay(
                        Image(systemName: "chevron.left")
                            .font(Font.body.weight(.bold))                        .foregroundColor(Color(hex:0x545454))
                    )
            }
        }
    }
}
